# LDAP Selfservice

This application is intended to be a selfservice application to give
users of a LDAP installation the possibilies to view and party edit their
stored userinformation.

## How it works?

The user logs in with his LDAP account. This Account is used
for all actions the user can take on his own userdata.
So if you dont want the user to see some information stored in his
LDAP entry, please make shure your LDAP ACLs are configured correctly.

## Configuration

The configuration of this application is completly done with environment
variables.

### LDAP Connection Settings

We need some information to be able to connect to the LDAP server:

    # LDAP server hostname or ip, not an URI
    # required
    LDAP_SELFSERVICE_LDAP_HOST
    # LDAP server port
    # optional, default: 389
    LDAP_SELFSERVICE_LDAP_PORT
    # Use TLS for connection encryption
    # optional, default: false, possible values: true/false
    LDAP_SELFSERVICE_LDAP_TLS
    # Use StartTLS for connection encryption
    # optional, default: false, possible values: true/false
    LDAP_SELFSERVICE_LDAP_STARTTLS

Additionally we need some information for user searching, so the user
can login with his username and does not need his full distinguished name.

    # Bind user DN
    # required
    LDAP_SELFSERVICE_LDAP_BIND_USER
    # Bind user password
    # optional, default: <empty>
    LDAP_SELFSERVICE_LDAP_BIND_PASSWORD
    # User search base
    # required 
    LDAP_SELFSERVICE_LDAP_USER_SEARCH_BASE
    # User search scope
    # optional, default: sub, possible values: base, one, sub
    LDAP_SELFSERVICE_LDAP_USER_SEARCH_SCOPE
    # User username attribute
    # optional, default: uid
    LDAP_SELFSERVICE_LDAP_UID_ATTRIBUTE
    # User search filter
    # optional, default: <empty>
    LDAP_SELFSERVICE_LDAP_USER_SEARCH_FILTER

### Application Branding

You are able to basically brand this application for your company.
It is possible to set you companys name and a (preferred absolut) path
to your company logo.

    LDAP_SELFSERVICE_COMPANY_NAME
    LDAP_SELFSERVICE_COMPANY_LOGO

## Powered by

* [Buffalo](http://gobuffalo.io)
* [Bootstrap](http://getbootstrap.com)
* [jQuery](http://jquery.com)
* [go-ldap](https://github.com/go-ldap/ldap/tree/v2.5.1)
* [go-crypt](https://github.com/GehirnInc/crypt)
