package models


// Credentials for user login.
type Credentials struct {
    Username string
    Password string
}
