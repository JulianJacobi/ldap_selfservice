package models

// User information model.
type User struct {
    GivenName string `ldap:"givenName"`
    Surname   string `ldap:"sn"`
    Username   string `ldap:"uid"`
    CommonName string `ldap:"cn"`
    UserID     string `ldap:"uidNumber"`
    GroupID    string `ldap:"gidNumber"`
    Mail       []string `ldap:"mail"`
    HomeDir    string `ldap:"homeDirectory"`
    Shell      string `ldap:"loginShell"`
}
