package grifts

import (
	"gitlab.com/JulianJacobi/ldap_selfservice/actions"

	"github.com/gobuffalo/buffalo"
)

func init() {
	buffalo.Grifts(actions.App())
}
