module gitlab.com/JulianJacobi/ldap_selfservice

go 1.14

require (
	github.com/GehirnInc/crypt v0.0.0-20200316065508-bb7000b8a962
	github.com/gobuffalo/buffalo v0.16.23
	github.com/gobuffalo/envy v1.9.0
	github.com/gobuffalo/mw-csrf v1.0.0
	github.com/gobuffalo/mw-forcessl v0.0.0-20200131175327-94b2bd771862
	github.com/gobuffalo/mw-i18n v1.1.0
	github.com/gobuffalo/mw-paramlogger v1.0.0
	github.com/gobuffalo/packr/v2 v2.8.1
	github.com/gobuffalo/suite v2.8.2+incompatible
	github.com/gobuffalo/validate v2.0.4+incompatible
	github.com/unrolled/secure v1.0.8
	golang.org/x/text v0.3.6 // indirect
	gopkg.in/asn1-ber.v1 v1.0.0-20181015200546-f715ec2f112d // indirect
	gopkg.in/ldap.v2 v2.5.1
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
