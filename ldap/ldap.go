package ldap

import (
    "fmt"
    "log"
    "strconv"
    "strings"

    "crypto/tls"

    "github.com/gobuffalo/buffalo"
	"github.com/gobuffalo/envy"

    "gopkg.in/ldap.v2"
)

var (
    host = envy.Get("LDAP_SELFSERVICE_LDAP_HOST", "")
    rawPort = envy.Get("LDAP_SELFSERVICE_LDAP_PORT", "398")
    port = convertPort(rawPort)
    tlsEnabled = strings.ToLower(envy.Get("LDAP_SELFSERVICE_LDAP_TLS", "false")) == "true"
    startTLS = strings.ToLower(envy.Get("LDAP_SELFSERVICE_LDAP_STARTTLS", "false")) == "true"
    bindDN = envy.Get("LDAP_SELFSERVICE_LDAP_BIND_USER", "")
    bindPassword = envy.Get("LDAP_SELFSERVICE_LDAP_BIND_PASSWORD", "")
    userSearchBase = envy.Get("LDAP_SELFSERVICE_LDAP_USER_SEARCH_BASE", "")
    userSearchScope = convertScope(envy.Get("LDAP_SELFSERVICE_LDAP_USER_SEARCH_SCOPE", "sub"))
    userSearchUidAttribute = envy.Get("LDAP_SELFSERVICE_LDAP_UID_ATTRIBUTE", "uid")
    userSearchFilter = envy.Get("LDAP_SELFSERVICE_LDAP_USER_SEARCH_FILTER", "")
)

// init function here checks correctness
// of some of the from environment obtained variables.
func init() {
    if len(host) == 0 {
        log.Fatalf("LDAP_SELFSERVICE_LDAP_HOST needed")
    }
    if len(bindDN) == 0 {
        log.Fatalf("LDAP_SELFSERVICE_LDAP_BIND_USER needed")
    }
}

// convertsPorts converts a given string to a int value,
// panics, if conversion fails.
func convertPort(s string) int {
    p, err := strconv.Atoi(s)
    if err != nil {
        log.Fatalf("LDAP_SELFSERVICE_LDAP_PORT \"%s\" is not a valid number.", s)
    }
    return p
}

// convertScope converts a given string to an valid ldap scope int.
// panics if invalid value is given
func convertScope(s string) int {
    switch s {
    case "base":
        return ldap.ScopeBaseObject
    case "one":
        return ldap.ScopeSingleLevel
    case "sub":
        return ldap.ScopeWholeSubtree
    case "":
        return ldap.ScopeWholeSubtree
    default:
        log.Fatalf("LDAP_SELFSERVICE_LDAP_USER_SEARCH_SCOPE \"%s\" is not a valid search scope, use one of: \"base\", \"one\", \"sub\"", s)
        return 0
    }
}


// GetCOnnection creates a LDAP connection from global configuration
func GetConnection() (*ldap.Conn, error) {
    var conn *ldap.Conn
    var err error
    var addr = fmt.Sprintf("%s:%d", host, port)
    var tlsConfig = &tls.Config{
        ServerName: host,
    }
    if tlsEnabled {
        conn, err = ldap.DialTLS("tcp", addr, tlsConfig)
        if err != nil {
            return nil, err
        }
    } else {
        conn, err = ldap.Dial("tcp", addr)
        if err != nil {
            return nil, err
        }
        err = conn.StartTLS(tlsConfig)
        if err != nil {
            return nil, err
        }
    }
    return conn, nil
}

// SearchUserDN searches for a user in configued ldap server
// with the given user seach configuration.
//
// returns usersDN if user is found, or nil pointer if user is not found.
func SearchUserDN(username string) (*string, error) {
    conn, err := GetConnection()
    if err != nil {
        return nil, err
    }
    if err := conn.Bind(bindDN, bindPassword); err != nil {
        return nil, err
    }
    searchRequest := ldap.NewSearchRequest(
        userSearchBase,
        userSearchScope, ldap.NeverDerefAliases, 0, 0, false,
        fmt.Sprintf(
            "(&(%s=%s)%s)", userSearchUidAttribute, username, userSearchFilter,
        ),
        []string{"dn"}, nil,
    )
    searchResult, err := conn.Search(searchRequest)
    if err != nil {
        return nil, err
    }
    if len(searchResult.Entries) == 0 {
        return nil, nil
    }
    return &searchResult.Entries[0].DN, nil
}

// BindedConnection returns a LDAP connection
// to the globaly configured server, 
// with the in the context logged in user binded.
func BindedConnection(c buffalo.Context) (*ldap.Conn, error) {
    conn, err := GetConnection()
    if err != nil {
        return nil, err
    }
    s := c.Session()
    username := s.Get("username").(string)
    password := s.Get("password").(string)
    if err := conn.Bind(username, password); err != nil {
        return nil, err
    }
    return conn, nil
}

// GetUserEntry gets the whole user entry
// of the in context logged in user
// from the gloally cofigured LDAP server.
//
// Set `op` to true, to additionally, get operational attributes.
func GetUserEntry(conn *ldap.Conn, c buffalo.Context, op bool) (*ldap.Entry, error) {
    userDN := c.Session().Get("username").(string)
    attrs := []string{"*"}
    if op {
        attrs = append(attrs, "+")
    }
    searchRequest := ldap.NewSearchRequest(
        userDN,
        ldap.ScopeBaseObject, ldap.NeverDerefAliases, 0, 0, false,
        "(&)", attrs, nil,
    )
    searchResult, err := conn.Search(searchRequest)
    if err != nil {
        return nil, err
    }
    if len(searchResult.Entries) == 0 {
        return nil, nil
    }
    return searchResult.Entries[0], nil
}
