package ldap

import (
    "reflect"

    "gopkg.in/ldap.v2"
)

// fieldsOfType returns aflat list of all fields of a type
// traversed over anonymous fields.
func fieldsOfType(t reflect.Type) []reflect.StructField {
    ret := make([]reflect.StructField, 0)
    for i := 0; i < t.NumField(); i++ {
        f := t.Field(i)
        if f.Anonymous {
            ret = append(ret, fieldsOfType(f.Type)...)
        } else {
            ret = append(ret, f)
        }
    }
    return ret
}

// FillFromEntry takes a pointer to a struct and a LDAP entry,
// and fills the struct with the information from LDAP entry.
//
// Use the `ldap` tag to map struct fields to LDAP attributes.
// Only fields with type `string` or `[]string` are filled with information.
func FillFromEntry(i interface{}, e *ldap.Entry) {
    v := reflect.ValueOf(i)
    for v.Kind() == reflect.Ptr {
        v = v.Elem()
    }
    for _, f := range fieldsOfType(v.Type()) {
        attr, ok := f.Tag.Lookup("ldap")
        if !ok {
            continue
        }
        vf := v.FieldByName(f.Name)
        if f.Type.Kind() == reflect.String {
            vf.SetString(e.GetAttributeValue(attr))
        }
        if f.Type.Kind() == reflect.Slice && f.Type.Elem().Kind() == reflect.String {
            vf.Set(reflect.ValueOf(e.GetAttributeValues(attr)))
        }
    }
}

