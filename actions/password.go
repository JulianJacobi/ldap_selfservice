package actions

import (
    "fmt"
	"net/http"

	"github.com/gobuffalo/buffalo"
    "github.com/gobuffalo/validate"

    "github.com/GehirnInc/crypt"
    _ "github.com/GehirnInc/crypt/sha512_crypt"

    "gitlab.com/JulianJacobi/ldap_selfservice/ldap"

    ldap_ "gopkg.in/ldap.v2"
)


type Passwords struct {
    OldPassword string
    NewPassword string
    NewPasswordRepeat string
}


// ChangePasswordHandler is a handler to change
// logged in user's password.
func ChangePasswordHandler(c buffalo.Context) error {
    errors := validate.NewErrors()
    if c.Request().Method == http.MethodPost {
        pws := &Passwords{}
        if err := c.Bind(pws); err != nil {
            return err
        }
        password := c.Session().Get("password").(string)
        if password != pws.OldPassword {
            errors.Add("old_password", T.Translate(c, "wrong_password"))
        }
        if pws.NewPassword != pws.NewPasswordRepeat {
            errors.Add("new_password", T.Translate(c, "different_passwords"))
            errors.Add("new_password_repeat", T.Translate(c, "different_passwords"))
        }
        if len(pws.NewPassword) == 0 {
            errors.Add("new_password", T.Translate(c, "empty_password"))
        }
        if !errors.HasAny() {
            crypt := crypt.SHA512.New()
            hash, err := crypt.Generate([]byte(pws.NewPassword), []byte{})
            if err != nil {
                return err
            }
            modRequest := ldap_.NewModifyRequest(c.Session().Get("username").(string))
            modRequest.Replace("userPassword", []string{fmt.Sprintf("{CRYPT}%s", hash)})
            conn, err := ldap.BindedConnection(c)
            if err != nil {
                return err
            }
            err = conn.Modify(modRequest)
            if err != nil {
                return err
            }
            c.Flash().Add("success", T.Translate(c, "changed_password"))
            return LogoutHandler(c)
        }
    }
    c.Set("pws", &Passwords{})
    c.Set("errors", errors)
	return c.Render(http.StatusOK, r.HTML("change_password.html"))
}

