package actions

import (
	"net/http"

	"github.com/gobuffalo/buffalo"

    "gitlab.com/JulianJacobi/ldap_selfservice/models"
    "gitlab.com/JulianJacobi/ldap_selfservice/ldap"
)

// HomeHandler is a default handler to serve up
// a home page containing basic user data information
func HomeHandler(c buffalo.Context) error {
    conn, err := ldap.BindedConnection(c)
    if err != nil {
        return err
    }
    entry, err := ldap.GetUserEntry(conn, c, false)
    if err != nil {
        return err
    }
    user := &models.User{}
    ldap.FillFromEntry(user, entry)
    c.Set("user", user)
	return c.Render(http.StatusOK, r.HTML("index.html"))
}

// RawHandler is a handler to serve up loggin in user's raw LDAP entry
func RawHandler(c buffalo.Context) error {
    conn, err := ldap.BindedConnection(c)
    if err != nil {
        return err
    }
    entry, err := ldap.GetUserEntry(conn, c, true)
    if err != nil {
        return err
    }
    c.Set("entry", entry)
	return c.Render(http.StatusOK, r.HTML("raw.html"))
}
