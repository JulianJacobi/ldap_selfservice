package actions

import (
    "log"
    "os"
	"github.com/gobuffalo/buffalo"
	"github.com/gobuffalo/envy"
	forcessl "github.com/gobuffalo/mw-forcessl"
	paramlogger "github.com/gobuffalo/mw-paramlogger"
	"github.com/unrolled/secure"

	csrf "github.com/gobuffalo/mw-csrf"
	i18n "github.com/gobuffalo/mw-i18n"
	"github.com/gobuffalo/packr/v2"

    "net/http"
)

// ENV is used to help switch settings based on where the
// application is being run. Default is "development".
var ENV = envy.Get("GO_ENV", "development")
var app *buffalo.App
var T *i18n.Translator
var companyName = envy.Get("LDAP_SELFSERVICE_COMPANY_NAME", "LDAP")
var companyLogo = envy.Get("LDAP_SELFSERVICE_COMPANY_LOGO", "")

// init function
func init() {
    // check if companyLogo is a valid path and a regular file
    if len(companyLogo) > 0 {
        logoInfo, err := os.Stat(companyLogo)
        if err != nil || !logoInfo.Mode().IsRegular() {
            log.Fatalf("Configured LDAP_SELFSERVICE_COMPANY_LOGO path \"%s\" seems not to be a regular file.", companyLogo)
        }
    }
}

// App is where all routes and middleware for buffalo
// should be defined. This is the nerve center of your
// application.
//
// Routing, middleware, groups, etc... are declared TOP -> DOWN.
// This means if you add a middleware to `app` *after* declaring a
// group, that group will NOT have that new middleware. The same
// is true of resource declarations as well.
//
// It also means that routes are checked in the order they are declared.
// `ServeFiles` is a CATCH-ALL route, so it should always be
// placed last in the route declarations, as it will prevent routes
// declared after it to never be called.
func App() *buffalo.App {
	if app == nil {
		app = buffalo.New(buffalo.Options{
			Env:         ENV,
			SessionName: "_ldap_selfservice_session",
		})

		// Automatically redirect to SSL
		app.Use(forceSSL())

		// Log request parameters (filters apply).
        paramlogger.ParameterExclusionList = append(
            paramlogger.ParameterExclusionList,
            "OldPassword",
            "NewPassword",
            "NewPasswordRepeat",
        )
		app.Use(paramlogger.ParameterLogger)

		// Protect against CSRF attacks. https://www.owasp.org/index.php/Cross-Site_Request_Forgery_(CSRF)
		// Remove to disable this.
		app.Use(csrf.New)

		// Setup and use translations:
		app.Use(translations())

        // Set company name and logo information in context
        app.Use(func (next buffalo.Handler) buffalo.Handler {
            return func (c buffalo.Context) error {
                c.Set("coName", companyName)
                c.Set("coLogo", (len(companyLogo) > 0))
                return next(c)
            }
        })

        // Protect endpoints with login requirement,
        // except the login endpont itself
        app.Use(LoggedInMiddleware)
        app.Middleware.Skip(LoggedInMiddleware, LoginHandler)

        // if logo is supplied, add logo endpoint,
        // otherwise add dummy endpoint (without this, templating breaks)
        logoHandler := func(c buffalo.Context) error {
            if len(companyLogo) > 0 {
                http.ServeFile(c.Response(), c.Request(), companyLogo)
            } else {
                return c.Error(404, nil)
            }
            return nil
        }
        app.Middleware.Skip(LoggedInMiddleware, logoHandler)
        app.GET("/logo/", logoHandler)

		app.GET("/", HomeHandler)
        app.GET("/raw/", RawHandler)

        app.GET("/login/", LoginHandler)
        app.POST("/login/", LoginHandler)
        app.GET("/logout/", LogoutHandler)
        app.GET("/change_password/", ChangePasswordHandler)
        app.POST("/change_password/", ChangePasswordHandler)

		app.ServeFiles("/", assetsBox) // serve files from the public directory
	}

	return app
}

// translations will load locale files, set up the translator `actions.T`,
// and will return a middleware to use to load the correct locale for each
// request.
// for more information: https://gobuffalo.io/en/docs/localization
func translations() buffalo.MiddlewareFunc {
	var err error
	if T, err = i18n.New(packr.New("app:locales", "../locales"), "en-US"); err != nil {
		app.Stop(err)
	}
	return T.Middleware()
}

// forceSSL will return a middleware that will redirect an incoming request
// if it is not HTTPS. "http://example.com" => "https://example.com".
// This middleware does **not** enable SSL. for your application. To do that
// we recommend using a proxy: https://gobuffalo.io/en/docs/proxy
// for more information: https://github.com/unrolled/secure/
func forceSSL() buffalo.MiddlewareFunc {
	return forcessl.Middleware(secure.Options{
		SSLRedirect:     ENV == "production",
		SSLProxyHeaders: map[string]string{"X-Forwarded-Proto": "https"},
	})
}
