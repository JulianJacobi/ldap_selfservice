package actions

import (
    "fmt"
    "log"
	"net/http"
    "net/url"

	"github.com/gobuffalo/buffalo"
    "github.com/gobuffalo/validate"

    "gitlab.com/JulianJacobi/ldap_selfservice/models"
    "gitlab.com/JulianJacobi/ldap_selfservice/ldap"
)

// LoginHandler is a handler to log in
// a user.
func LoginHandler(c buffalo.Context) error {
    s := c.Session()
    // Check if user is already correctly logged in.
    if s.Get("username") != nil && s.Get("password") != nil {
        return c.Redirect(http.StatusSeeOther, "rootPath()")
    }

    creds := &models.Credentials{}
    if c.Request().Method == http.MethodPost {
        if err := c.Bind(creds); err != nil {
            return err
        }
        conn, err := ldap.GetConnection()
        if err != nil {
            return err
        }
        defer conn.Close()
        userDN, err := ldap.SearchUserDN(creds.Username)
        if err != nil {
            return err
        }
        errors := validate.NewErrors()
        if userDN == nil {
            // User not found
            log.Printf("User \"%s\" not found.", creds.Username)
            errors.Add("username", T.Translate(c, "login_failed"))
            errors.Add("password", "")
        } else {
            if err := conn.Bind(*userDN, creds.Password); err != nil {
                // User bind failed (password wrong).
                log.Printf("Password wrong for user \"%s\"", *userDN)
                errors.Add("username", T.Translate(c, "login_failed"))
                errors.Add("password", "")
            } else {
                // Successfully logged in user
                s.Set("username", *userDN)
                s.Set("password", creds.Password)
                redirect_url := c.Request().Form.Get("redirect_url")
                if len(redirect_url) == 0 {
                    redirect_url = "rootPath()"
                }
                return c.Redirect(http.StatusSeeOther, redirect_url)
            }
        }
        c.Set("errors", errors)
    }
    creds.Password = ""
    c.Set("creds", creds)
	return c.Render(http.StatusOK, r.HTML("login.html"))
}

// LogoutHandler is a handler to log out
// a user.
func LogoutHandler(c buffalo.Context) error {
    s := c.Session()
    s.Delete("username")
    s.Delete("password")
    s.Save()
    c.Flash().Add("success", T.Translate(c, "logged_out"))
    return c.Redirect(http.StatusSeeOther, "loginPath()")
}

// LoggedInMiddleware is a middleware that
// redirects unauthenticates requests to login.
//
// The the originally requested path is inserted
// as `redirect_url` parameter in the redirect url
func LoggedInMiddleware(next buffalo.Handler) buffalo.Handler {
    return func(c buffalo.Context) error {
        s := c.Session()
        if s.Get("username") == nil || s.Get("password") == nil {
            reqUrl := c.Request().URL
            redirect_url := reqUrl.Path
            if len(reqUrl.RawQuery) > 0 {
                redirect_url = fmt.Sprintf("%s?%s", redirect_url, reqUrl.RawQuery)
            }
            return c.Redirect(
                http.StatusSeeOther,
                "/login/?redirect_url=%s",
                url.QueryEscape(redirect_url),
            )
        }

        return next(c)
    }
}
